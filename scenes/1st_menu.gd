
extends TextureFrame
var help_pressed = 1
func _on_play_pressed():
	get_node("/root/loader").goto_scene("res://scenes/2nd_menu.tscn")


func _on_exit_pressed():
	get_tree().quit()


func _on_help_pressed():
	get_node("title_animation").play("title_clear", 1, help_pressed)
	get_node("help_animation").play("basis_help", 1, help_pressed)
	help_pressed = -help_pressed
