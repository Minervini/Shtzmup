
extends TextureFrame

onready var loader = get_node("/root/loader")

func _on_resume_pressed():
	get_tree().set_pause(false)
	get_parent().remove_child(self)

func _on_settings_pressed():
	pass # replace with function body


func _on_quit_pressed():
	get_tree().set_pause(false)
	get_tree().quit()


func _on_help_pressed():
	pass # replace with function body


func _on_menu_pressed():
	get_tree().set_pause(false)
	loader.goto_scene("res://scenes/1st_menu.tscn")
