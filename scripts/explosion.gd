
extends Area2D


onready var scale = get_scale()
var expension_velocity = Vector2(1.005,1.005)
var time = 0
var apex = 0
var m_aoe = 5.0
var m_dmg = 0
var alpha = 1
var decr_speed = 0.015
var velocity = Vector2(-1.3, 0)

func _ready():
	set_fixed_process(true)
	var rot = rand_range(-1,1)
	set_rot(rot)
	pass

func _fixed_process(delta):
	translate(velocity)
	time += delta
	alpha -= decr_speed
	set_opacity(alpha)
	if scale.x > 0:
		if scale.x < m_aoe / 10 and apex == 0 :
			scale *= expension_velocity * 2.5
			
		else:
			apex = 1
			scale /= expension_velocity 
		set_scale(scale)
		
		if is_monitoring_enabled():
			var ob = get_overlapping_bodies()
		
			for i in ob:
				#print(i.get_name())
				if i.is_in_group("enemies"):
					deal_dmg(i)
				if i.is_in_group("collectable"):
					i.destroy()
		if time > 0.1:
			set_enable_monitoring(false)
	
		if scale.x < 0.01 or alpha < 0.00001:
			destroy()

func _set_position(pos):
	set_pos(pos)


func set_aoe(aoe):
	m_aoe = aoe
	
func set_dmg(dmg):
	m_dmg = dmg
	
func deal_dmg(collider, dmg = m_dmg):
	#print(collider.get_name())
	collider.attacked(dmg)
	
func destroy():
	queue_free()