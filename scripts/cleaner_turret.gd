
extends StaticBody2D

var laser_scene
var player_rot
var lock_time = 0
var offset = 1

func _ready():
	set_rot(0)
	laser_scene = load("res://cleaner_laser.scn")

func player_spotted(pos, dmg):
	lock_time += offset
	#player_rot = get_parent().get_player().get_rot()
	var glob_pos = get_global_pos()
	#print(get_angle_to(pos))
	#set_rot(get_angle_to(pos))
	set_rot(PI/2 * lock_time / 100)
	print(lock_time)
	if abs(lock_time) == 30:
		laser_shoot(dmg)
		offset = -offset
	#look_at(Vector2(-pos.y, pos.x))
	
func laser_shoot(dmg):
	var shot = laser_scene.instance()
	add_child(shot)
	shot.set_pos(Vector2(get_pos().x, get_pos().y - 35 ))
	shot.set_dmg(dmg)
	#shot.set_rot(get_parent().get_node("turret_part2").get_rot())
	