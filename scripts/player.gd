extends KinematicBody2D

const GRAVITY = 200
const MOVE_SPEED = 400

var smoke_scene
var ammos_scene
var player


var m_Hp = 100
var m_Shield = 100
var velocity = Vector2()
var time = 0
var shot_time = 0
var fire_rate = 0

var current_weapon
var last_pos
var weapons = []
var weapons_indice = 0
var weapons_number = 3
var angle = 0
var released = 0
var propeller_opacity = 0
var ship_length = 5
var weapon_pos_y = 0.5
var smoke
var turned = 1
var weapon_way = 1
var left = 0
var right = 1

onready var g_x = get_global_pos().x
onready var g_y = get_global_pos().y
onready var shield_bar = get_node("shield_bar")
onready var shield_bar_1_texture = load("res://shield_bar.png")
onready var shield_bar_2_texture = load("res://shield_bar-2.png")
onready var max_Hp = get_node("/root/global").get_max_hp_player()
onready var max_Shield = get_node("/root/global").get_max_shield_player()
onready var game_state = get_node("/root/game_state")
onready var entities = get_node("/root/entities")

func _fixed_process(delta):
	g_x = get_global_pos().x
	g_y = get_global_pos().y
	if m_Hp <= 0:
		get_parent().loose()
	#print(get_pos())
	time += delta
	
	if int(time) % 10 == 0 or time == 0.0:
		last_pos = get_pos()
	#print(time)
	#print(str(get_pos()))
	velocity.y += delta * GRAVITY
	if not Input.is_action_pressed("player_down") and not Input.is_action_pressed("player_up"):
		#print("test")
		look_normal()

	if (Input.is_action_pressed("player_left")):
		propeller_adjust("<", 0.5, 0.1)
		velocity.x = - MOVE_SPEED
	elif (Input.is_action_pressed("player_right")):
		propeller_adjust("<", 1, 0.1)
		velocity.x =   MOVE_SPEED
	else:
		propeller_adjust(">", 0, -0.1)
		velocity.x = 0
		
	if (Input.is_action_pressed("player_up")):
		propeller_adjust("<", 1, 0.2)
		look_up()
		velocity.y = - MOVE_SPEED
	elif (Input.is_action_pressed("player_down")):
		propeller_adjust("<", 1, 0.2)
		look_down()
		velocity.y = MOVE_SPEED
	else:
		velocity.y = 0

	
	if (Input.is_action_pressed("ui_left")) and right == 1:
		left = 1
		right = 0
		get_node("ship").set_flip_h(true)
		get_node("propeller").set_flip_h(true)
		get_node("weapon").set_flip_h(true)
		get_node("weapon").translate(Vector2(-53, 0))
		shield_bar.set_progress_texture(shield_bar_2_texture)
		shield_bar.set_fill_mode(0)
		weapon_way = -1
	if (Input.is_action_pressed("ui_right")) and left == 1:
		right = 1
		left = 0
		get_node("ship").set_flip_h(false)
		get_node("propeller").set_flip_h(false)
		get_node("weapon").set_flip_h(false)
		get_node("weapon").translate(Vector2(53, 0))
		shield_bar.set_progress_texture(shield_bar_1_texture)
		shield_bar.set_fill_mode(1)
		weapon_way = 1

	elif not (Input.is_action_pressed("player_turn")):
		turned = 1
		
	if (Input.is_action_pressed("player_shot")):
		if (time - shot_time > fire_rate) and weapons[weapons_indice][2] != 0:
			shot_time = time
			var ammo = current_weapon.instance()
			ammo.add_to_group("projectile")
			get_parent().add_child(ammo)
			ammo.set_pos(Vector2(get_pos().x + ship_length * weapon_way, get_pos().y - weapon_pos_y - get_rot() * 5))
			fire_rate = ammo.get_fire_rate()
			ammo.set_dmg(weapons[weapons_indice][4])
			if weapons[weapons_indice][2] > 0:
				weapons[weapons_indice][2] -= 1
				get_node("ammo").set_text(str(weapons[weapons_indice][2]))
			
	if (Input.is_action_pressed("ui_down") and released == 0):
		change_weapon()
		released = 1
	elif not Input.is_action_pressed("ui_down"):
		released = 0
		
	if m_Hp < 100:
		if not get_parent().has_node("Smoke"):
			smoke = smoke_scene.instance()
			get_parent().add_child(smoke)

		else:
			smoke.set_amount(100 - m_Hp)
			smoke.set_param(12, (100 - m_Hp)/ 10)
			smoke.set_pos(Vector2(get_pos().x - 5, get_pos().y - 2))
	else:
		if get_parent().has_node("Smoke"):
			smoke.queue_free()
	var motion = velocity * delta
	motion = move(motion)

	move(motion)

func _ready():
	give_shield(0)
	smoke_scene = load("res://smoke.scn")
	add_to_group("players")
	get_node("propeller").set_opacity(propeller_opacity)
	weapons = entities.get_weapons()
	change_ammo()
	
	current_weapon = weapons[0][0]
	get_node("Hp").set_text(str(m_Hp))
	set_fixed_process(true)

func _get_last_pos():
	return last_pos

func attacked(dmg_amount):
	if m_Shield > 0:
		give_shield(-dmg_amount)
	else:
		get_parent().reset_multiplier()
		m_Hp = m_Hp - dmg_amount
		if m_Hp <= 0:
			m_Hp = 0
		get_node("Hp").set_text(str(m_Hp))
	
func look_up():
	
	if angle <= PI/4 * weapon_way:
		angle += 0.05 * weapon_way
		set_rot(angle)

func look_down():
	if angle >= -PI/4 * weapon_way:
		angle -= 0.05 * weapon_way
		set_rot(angle)

func look_normal():
	if angle > 0:
		angle -= 0.1
	if angle < 0:
		angle += 0.1
	set_rot(angle)
	
func propeller_adjust( sup_inf, lim, delta):
	if sup_inf == ">":
		if propeller_opacity > lim:
			propeller_opacity += delta
			get_node("propeller").set_opacity(propeller_opacity)
	elif sup_inf == "<":
		if propeller_opacity < lim:
			propeller_opacity += delta
			get_node("propeller").set_opacity(propeller_opacity)
			
func change_ammo():
	var ammo = get_node("ammo")
	var logo = get_node("ammo_logo")
	if weapons[weapons_indice][2] >= 0:
		ammo.set_text(str(weapons[weapons_indice][2]))
		if ammo.get_rotation() != 0:
			ammo.set_rotation(0)
			ammo.set_pos(Vector2(logo.get_pos().x - 30, logo.get_pos().y - 6))
	else:
		ammo.set_text("8")
		ammo.set_pos(Vector2(logo.get_pos().x - 30, logo.get_pos().y + 5))
		ammo.set_rotation(PI/2)

func change_weapon():
	if weapons_indice == weapons_number - 1:
		weapons_indice = 0
	else:
		weapons_indice += 1
	current_weapon = weapons[weapons_indice][0]
	get_node("weapon").set_texture(weapons[weapons_indice][1])
	change_ammo()
	#print(current_weapon)
	
func refil_ammo(weapon, amount):
	if weapons[weapon][2] >= 0:
		weapons[weapon][2] += amount
		if weapons[weapon][2] > weapons[weapon][3]:
			weapons[weapon][2] = weapons[weapon][3]
		if weapon == weapons_indice:
			get_node("ammo").set_text(str(weapons[weapons_indice][2]))
		
func get_hp():
	return m_Hp
	
func set_shield(amount):
	m_Shield = amount
	shield_bar.set_value(m_Shield)
	
func give_shield(amount):
	m_Shield = m_Shield + amount
	if m_Shield < 0:
		m_Shield = 0
	shield_bar.set_value(m_Shield)
	
func get_shield():
	return m_Shield

func set_hp(amount):
	if m_Shield > 0:
		set_shield(-amount)
	else:
		m_Hp = m_Hp - amount
		if m_Hp > max_Hp:
			m_Hp = max_Hp

func get_dir():
	return weapon_way