
extends Node

var player_hp = 100
var player_shield = 100

func set_pHp(hp):
	player_hp = hp
	
func get_pHp():
	return player_hp


func set_pS(s):
	player_shield = s
	
func get_pS():
	return player_shield
