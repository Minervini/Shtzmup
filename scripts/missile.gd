
extends KinematicBody2D
var dir = Vector2(1,0)
var velocity = 10.0
var m_dmg = 200
var aoe = 2.0
var explosion_scene
var fire_rate = 1
var ammo_max = 20
onready var m_Hp = get_node("/root/entities").get_weapons()[1][5]

func _ready():
	var player_dir = get_parent().get_node("player").get_dir()
	dir *= player_dir
	var rot = get_parent().get_node("player").get_rot()
	get_node("Sprite").set_flip_h((player_dir - 1)/2)
	set_rot(rot)
	dir.y = -(rot + dir.y)
	explosion_scene = load("res://explosion.scn")
	add_collision_exception_with(get_parent().get_node("player"))
	set_fixed_process(true)
	pass

func _fixed_process(delta):
	move(dir * velocity)
	if is_colliding():
		if get_collider().is_in_group("e_p"):
			get_collider().attacked(m_dmg)
		elif not get_collider().is_in_group("players") and not get_collider().is_in_group("collectable"):
			explode()
	if get_pos().x > 200:
		destroy()

func explode():
	set_hidden(true)
	var explosion = explosion_scene.instance()
	get_parent().add_child(explosion)
	explosion.set_pos(Vector2(get_pos().x + aoe * 3, get_pos().y))
	explosion.set_aoe(aoe)
	explosion.set_dmg(m_dmg)
	destroy()
	
func attacked(dmg):
	m_Hp = m_Hp - dmg
	
func get_dmg():
	return m_dmg
	
func destroy():
	queue_free()
	
func get_fire_rate():
	return fire_rate
	
func set_dmg(dmg):
	m_dmg = dmg