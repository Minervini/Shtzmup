extends Node

var weapons = [
[load("res://projectile.scn"), load("res://weapon-1.png"), -1, -1, 15, 1000],
[load("res://missile.scn"), load("res://weapon-2.png"), 0, 20, 200, 1000],
[load("res://laser.scn"), load("res://weapon-3.png"), 0, 1000, 15, -1]
]

var ammo_amounts = [-1, 5, 100]

var collectable_items = [
[load("res://health.scn"), 1000],
[load("res://shield.scn"), 2000],
[load("res://ammo-1.scn"), 700],
[load("res://ammo-2.scn"), 1500]
]

var enemies = {
"Peon" : {
"ammos" : [load("res://enemy_projectile.scn")], 
"explosion" : load("res://ship_destruction.scn"), 
"fire_rate" : 1.5,
"hp" : 100,
"speed" : Vector2(5, 0.5),
"suicide_dmg" : 25,
"ammo_0_parameters" : [5, 1]

},
"Interceptor" : {
"ammos" : [load("res://interceptor_projectile.scn")], 
"explosion" : load("res://ship_destruction.scn"), 
"fire_rate" : 0.2,
"hp" : 15,
"speed" : Vector2(10, 2),
"suicide_dmg" : 10,
"ammo_0_parameters" : [1, 1]
},
"Cleaner" : {
"ammos" : [load("res://cleaner_projectile.scn"), load("res://cleaner_laser.scn")], 
"explosion" : load("res://ship_destruction.scn"), 
"fire_rate" : 0.01,
"hp" : 150,
"speed" : Vector2(5, 2),
"suicide_dmg" : 30,
"ammo_0_parameters" : [2, 1],
"ammo_0_parameters" : [20, -1]
}
}

func get_enemies():
	return enemies
	
func get_weapons():
	return weapons
	
func set_weapons(w):
	weapons = w

func get_collectable():
	return collectable_items

