
extends KinematicBody2D

var velocity = 15
var m_dir = Vector2(1,0)
var m_dmg = 0
onready var m_Hp = get_node("/root/entities").get_enemies()["Peon"]["ammo_0_parameters"][1]

func _ready():
	add_to_group("e_p")
	set_fixed_process(true)
	pass

func _set_dir(dir):
	m_dir = dir
	
func _set_pos(pos):
	set_pos(pos)

func _fixed_process(delta):
	if m_Hp == 0:
		destroy()
	move(m_dir * velocity)
	if is_colliding():
		#print(get_collider())
		if get_collider().is_in_group("enemy_projectile"):
			add_collision_exception_with(get_collider())
		elif get_collider().is_in_group("enemies"):
			destroy()
		elif get_collider().is_in_group("a_p"):
			get_collider().attacked(m_dmg)
		elif get_collider().is_in_group("players"):
			get_collider().attacked(m_dmg)
			destroy()
		else:
			destroy()
func attacked(dmg):
	m_Hp = m_Hp - dmg
	

func destroy():
	queue_free()

func set_dmg(dmg):
	m_dmg = dmg