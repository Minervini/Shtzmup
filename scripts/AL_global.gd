extends Node

var score = 0
var max_Hp_player = 100
var max_Shield_player = 100
var player_level = 1
var xp = 0
onready var xp_till_nxt_lvl = player_level * 1000 + pow(10, player_level)
#player level param
func get_plvl():
	return player_level
	
func set_plvl(plvl):
	 player_level = plvl
	
func get_xp():
	return xp
	
func get_missing_xp():
	return xp_till_nxt_lvl
	
func set_xp(n_xp):
	xp = n_xp
	
#Shield player param
func get_max_shield_player():
	return max_Shield_player

func set_max_shield_player(new_mspp):
	max_Shield_player = new_mspp

#Hp player param
func set_max_hp_player(new_mhpp):
	max_Hp_player = new_mhpp

func get_max_hp_player():
	return max_Hp_player

#Player score param	
func set_score(new_score):
	score = new_score
	
func get_score():
	return score