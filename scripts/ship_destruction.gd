
extends Sprite

var decr_velocity = Vector2(1.05, 1.05)

onready var new_scale = get_scale()
onready var opacity = get_opacity()

func _ready():
	var rot = rand_range(-1,1)
	set_rot(rot)
	set_fixed_process(true)
	pass

func _fixed_process(delta):
	new_scale /= decr_velocity
	opacity -= 0.05
	set_opacity(opacity)
	set_scale(new_scale)
	if opacity == 0:
		destroy()
	
func destroy():
	queue_free()