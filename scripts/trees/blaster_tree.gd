
extends TextureFrame

var drag_speed = 0
onready var blaster = get_node("/root/entities").get_weapons()[0]
func _ready():
	set_fixed_process(true)
	pass

func _fixed_process(delta):
	if Input.is_mouse_button_pressed(1):
		drag_speed = Input.get_mouse_speed().y

		if get_parent().get_pos().y <= 1150 and get_parent().get_pos().y >= 0:
			get_parent().translate(Vector2(0,drag_speed / 50))
		elif get_parent().get_pos().y > 1150:
			get_parent().set_pos(Vector2(0,1150))
		elif get_parent().get_pos().y < 0:
			get_parent().set_pos(Vector2(0,0))

func _on_resistance_pressed():
	pass # replace with function body


func _on_size_pressed():
	pass # replace with function body


func _on_spray_pressed():
	pass # replace with function body


func _on_as_pressed():
	pass # replace with function body


func _on_dmgs_pressed():
	pass # replace with function body


func _on_pierce_pressed():
	pass # replace with function body


func _on_guide_pressed():
	pass # replace with function body


func _on_dual_pressed():
	pass # replace with function body


func _on_trial_pressed():
	pass # replace with function body
