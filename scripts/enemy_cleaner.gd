
extends KinematicBody2D


onready var entities = get_node("/root/entities")
onready var self_param = entities.get_enemies()["Cleaner"]
onready var fire_rate = self_param["fire_rate"]
onready var bullet_scene = self_param["ammos"][0]
onready var m_Hp = self_param["hp"]
onready var m_Hp_max = m_Hp
onready var speed = self_param["speed"]
onready var destruction_scene = self_param["explosion"]
onready var points = m_Hp
onready var m_dmg = self_param["suicide_dmg"]

var player
var player_pos
var dir = Vector2(-1,0)


var time = 0

var shot_time = 0

var explosion_variance = 3

func _ready():
	get_node("hp_bar").set_max(m_Hp_max)
	get_node("hp_bar").set_value(m_Hp * 100 / m_Hp_max)
	player = get_parent().get_node("player")
	add_collision_exception_with(get_parent().get_node("back_blocker"))
	add_collision_exception_with(get_parent().get_node("bottom_blocker"))

	add_collision_exception_with(get_node("turret_part2"))
	#print(player.get_pos())
	set_fixed_process(true)
	pass
	
func _fixed_process(delta):
	
	#print(fire_rate)
	time += delta
	if player.get_pos().x < get_pos().x and player.get_pos().y - 15 > get_pos().y:
		get_node("turret_part2").player_spotted(player.get_pos(), self_param["ammo_1_parameters"][0])
	if m_Hp <= 0:
		scored()
		
	var dir_x = (get_pos().x - player._get_last_pos().x)/abs(get_pos().x - player._get_last_pos().x)
	var dir_y = (get_pos().y - player._get_last_pos().y)/abs(get_pos().y - player._get_last_pos().y)
	#print("( ",dir_x," , ",dir_y," )")
	#print("time : ", int(time))
	if (time - shot_time > fire_rate):
		shot_time = time
		shoot()
		
	if abs(get_pos().x - player.get_pos().x) == 0:
		dir_x = 0
		
	if abs(get_pos().y - player._get_last_pos().y) == 0:
		#print(get_pos().y - player._get_last_pos().y)
		dir_y = 0
	#dir = Vector2(dir_x, dir_y)
	dir = Vector2(-abs(dir_x), -dir_y)
	if get_pos().x < -24:
		dir = Vector2(-5,0)
		
	var motion = dir * speed
	if not (get_pos().x < -50) and not (get_pos().y > 70):
		move(motion)
	else:
		destroy()
		
	if is_colliding():
		if get_collider().is_in_group("players"):
			get_collider().attacked(m_dmg)
			attacked(m_Hp)
			
		if get_collider().is_in_group("collectable"):
			add_collision_exception_with(get_collider())
			
		if get_collider().is_in_group("enemies") or get_collider().is_in_group("undestructible"):
			var n = get_collision_normal()
			motion = n.slide(motion)
			speed = n.slide(speed)
			move(motion)
			
		
		

func shoot():
	for i in range(0,3):
		var bullet = bullet_scene.instance()
		get_parent().add_child(bullet)
		bullet._set_dir(Vector2(dir.x, 0))
		bullet._set_pos(Vector2(get_pos().x - i * 2, get_pos().y + i * 2))
		bullet.add_to_group("enemy_projectile")
		bullet.set_offset(i)
		bullet.set_dmg(self_param["ammo_0_parameters"][0])
		add_collision_exception_with(bullet)
	
func destroy():
	remove_from_group("enemies")
	get_parent().remove_enemy()
	queue_free()

func attacked(dmg):
	m_Hp = m_Hp - dmg
	get_node("hp_bar").set_value(m_Hp * 100 / m_Hp_max)
	
func scored():
	set_hidden(true)
	for i in range(0,explosion_variance):
		var destr = destruction_scene.instance()
		get_parent().add_child(destr)
		destr.set_pos(get_pos())
	var score = int((points + m_dmg)/fire_rate)
	#print(score)
	get_parent().scoring(score)
	destroy()

func get_player():
	return player
	
func set_fire_rate(f_r):
	fire_rate = f_r
func get_fire_rate():
	return fire_rate
	
	