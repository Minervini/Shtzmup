
extends Area2D
var time = 0
func _ready():
	set_process(true)
	pass

func _process(delta):
	time += delta * 10
	if int(time) % 2 == 0:
		var sight = check_sight()
		if  sight != Vector2(-1, -1):
			get_parent().get_node("turret_part2").player_spotted(sight)

func check_sight():
	var detected_bodies = get_overlapping_bodies()
	for i in detected_bodies:
		if i.is_in_group("players"):
			return i.get_pos()
	return Vector2(-1, -1)