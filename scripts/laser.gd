
extends Area2D


var m_dmg = 15
var fire_rate = 0.01

func _ready():
	var rot = get_parent().get_node("player").get_rot() + (get_parent().get_node("player").get_dir() - 1)/2 * PI
	set_rot(rot)
	set_fixed_process(true)
	pass

func _fixed_process(delta):
	if is_monitoring_enabled():
			var ob = get_overlapping_bodies()
		
			for i in ob:
				#print(i.get_name())
				if i.is_in_group("enemies"):
					deal_dmg(i)
				if i.is_in_group("collectable"):
					i.destroy()
			set_enable_monitoring(false)
	destroy()

func get_fire_rate():
	return fire_rate

func destroy():
	queue_free()

func deal_dmg(collider, dmg = m_dmg):
	#print(collider.get_name())
	collider.attacked(dmg)

func set_dmg(dmg):
	m_dmg = dmg