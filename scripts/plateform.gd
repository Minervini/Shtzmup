
extends KinematicBody2D

func _ready():
	set_fixed_process(true)
	pass
	
func _fixed_process(delta):
	var dir = Vector2(-5,0)
	if is_colliding():
		if get_collider() == get_parent().get_node("player"):
			#print("player")
			dir.y += 50
	#print(get_pos())
	if not (get_pos().x < -50) and not (get_pos().y > 70):
		move(dir)
	else:
		free()
