
extends TextureButton

onready var base_color = get_modulate()

func _ready():
	set_process(true)
	pass

func _process(delta):
	if is_hovered():
		set_modulate("00a5ff")
	else:
		set_modulate(base_color)

