
extends ParallaxBackground

var scrolling_speed = Vector2(-2.5, 0)
var offset = Vector2(0, 0)

func _ready():
	set_fixed_process(true)
	set_scroll_base_scale(Vector2(1,1))
	pass

func _fixed_process(delta):
	offset += scrolling_speed
	set_scroll_base_offset(offset)

func _set_scr_speed(speed):
	scrolling_speed = speed
func _set_offset(offset_new):
	offset = offset_new
	
func _get_offset():
	return offset