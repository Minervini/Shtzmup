extends Node2D

var health_scene
var shield_scene
var ammo_1_scene
var ammo_2_scene
var pause_menu
var player
var plat_pos = Vector2(50, 50)
var time = 0
var enemy_number = 0
var max_enemies = 3
var multiplier = 1
var wave = 0

var nbr_enemy_up_cadence = 500
onready var game_variables = get_node("/root/global")
onready var enemies_grid = get_node("/root/enemies").get_enemy_grid(0)
onready var collectables = get_node("/root/entities").get_collectable()
onready var score = game_variables.get_score()
onready var screen_size = get_viewport_rect().size
onready var middle = Vector2((screen_size.x - 700) /2 , (screen_size.y - 685)/ 2)
func _ready():
	#newB_scene = load("res://enemy.scn")
	#newI_scene = load("res://interceptor.scn")
	#newC_scene = load("res://enemy_cleaner.scn")
	health_scene = load("res://health.scn")
	shield_scene = load("res://shield.scn")
	ammo_1_scene = load("res://ammo-1.scn")
	ammo_2_scene = load("res://ammo-2.scn")
	pause_menu = load("res://scenes/pause_menu.tscn")
	player = get_node("player")
	#var newP = newP_scene.instance()
	
	set_fixed_process(true)
	pass

	
func _fixed_process(delta):
	get_node("wave").set_text(str(wave))
	if Input.is_action_pressed("pause"):
		var pause = pause_menu.instance()
		get_tree().get_root().add_child(pause)
		pause.set_pos(middle)
		print(pause.get_pos())
		get_tree().set_pause(true)
	time += 1
	
	if time % 100 == 0 and enemy_number < max_enemies :
		wave += 1
		use_enemy_grid(enemies_grid[0])
	for i in collectables:
		spawn_collectable(i[0], i[1])


func get_score():
	return score
	
func remove_enemy():
	enemy_number -= 1
	
func scoring(pts):
	score += pts * multiplier
	#print(score)
	get_node("score").set_text(str(score))
	multiplier += 1
	get_node("multiplier").set_text("x" + str(multiplier))
	game_variables.set_score(score)
	
func loose():
	get_node("/root/loader").goto_scene("res://lost.scn")
	
func reset_multiplier():
	multiplier = 1
	get_node("multiplier").set_text("x" + str(multiplier))

func spawn_enemy(scene, pos):
	var newE = scene.instance()
	add_child(newE)
	newE.set_pos(pos)
	newE.add_to_group("enemies")
	#newE.set_fire_rate(1 - log(wave)/10)
	
	if newE.get_fire_rate() < 0.1:
		newE.set_fire_rate(0.1)
		
func use_enemy_grid(enemy_grid):
	for i in enemy_grid:
		if i[1] <= wave:
			#print(i[1], wave)
			for j in range(i[2]):
				spawn_enemy(i[0], Vector2(70, rand_range(-26, 35)))
				enemy_number += 1
		
func spawn_collectable(item, loot_chance):
	if int(rand_range(0,loot_chance + 1)) == loot_chance:
		var new_item = item.instance()
		add_child(new_item)
		new_item.set_pos(Vector2(70, rand_range(-26, 35)))