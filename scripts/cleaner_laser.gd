
extends Area2D


var m_dmg = 20
var fire_rate = 0.01
var m_rot = 0
var time = 0
func _ready():
	set_rot(PI/4)
	set_fixed_process(true)
	pass

func _fixed_process(delta):
	time += delta * 10
	if is_monitoring_enabled():
		var ob = get_overlapping_bodies()
	
		for i in ob:
			#print(i.get_name())
			if i.is_in_group("players"):
				deal_dmg(i)
				set_enable_monitoring(false)
	if int(time) % 5 == 4:
		destroy()

func get_fire_rate():
	return fire_rate

func destroy():
	queue_free()

func deal_dmg(collider, dmg = m_dmg):
	#print(collider.get_name())
	collider.attacked(dmg)
	
func set_dmg(dmg):
	m_dmg = dmg