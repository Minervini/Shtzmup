
extends KinematicBody2D
var m_dir = Vector2(1,0)
var velocity = 10.0
var m_dmg = 0
var fire_rate = 0.1
var time = 0
var trace_scene
var trace
var m_offset = 0
onready var m_Hp = get_node("/root/entities").get_enemies()["Cleaner"]["ammo_0_parameters"][1]

func _ready():
	add_to_group("e_p")
	set_fixed_process(true)
	pass

func _fixed_process(delta):
	if m_Hp == 0:
		destroy()
		
	time += delta * 10
	m_dir.y = sin(time + m_offset) / 2
	set_rot(m_dir.y)
	move(m_dir * velocity)
	if is_colliding():
		if get_collider().is_in_group("players"):
			get_collider().attacked(m_dmg)
			destroy()
		else:
			if get_collider().is_in_group("undestructible"):
				destroy()
			elif get_collider().is_in_group("a_p"):
				get_collider().attacked(m_dmg)
			else:	
				add_collision_exception_with(get_collider())
		
func attacked(dmg):
	m_Hp = m_Hp - dmg

func get_fire_rate():
	return fire_rate
	
func set_offset(offset):
	m_offset = offset
	
func destroy():
	queue_free()
	
func _set_dir(dir):
	m_dir = dir
	
func _set_pos(pos):
	set_pos(pos)

func set_dmg(dmg):
	m_dmg = dmg