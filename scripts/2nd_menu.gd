
extends TextureFrame
onready var game_variables = get_node("/root/global")
onready var p_lvl = game_variables.get_plvl()
func _ready():
	get_node("middle_content/leveling/level").set_text(str(p_lvl))
	pass


func _on_exit_pressed():
	get_tree().quit()


func _on_menu_pressed():
	get_node("/root/loader").goto_scene("res://scenes/1st_menu.tscn")


func _on_help_pressed():
	pass


func _on_settings_pressed():
	pass


func _on_left_pressed():
	pass


func _on_right_pressed():
	pass


func _on_start_pressed():
	get_node("/root/loader").goto_scene("res://scene_1.scn")


func _on_skill_tree_pressed():
	get_node("/root/loader").goto_scene("res://scenes/skill_tree.tscn")
