extends KinematicBody2D

var rot = 0
var dir = Vector2(-1,0)
var velocity = 3
var bonus_pts = 1000
var health_amount = 25
func _ready():
	add_to_group("collectable")
	set_fixed_process(true)
	pass

func _fixed_process(delta):
	move(dir * velocity)
	rot += 0.05
	set_rot(rot)
	if is_colliding():
		if get_collider().is_in_group("players"):
			if get_collider().get_hp() < 100:
				get_collider().attacked(-health_amount)
			else:
				get_parent().scoring(bonus_pts)
			destroy()
		else:
			add_collision_exception_with(get_collider())

func _set_pos(pos):
	set_pos(pos)

func _get_pos():
	return get_pos()
	
func destroy():
	queue_free()
