extends Node

var max_enemies
var enemies_scenes = [ load("res://enemy.scn"), load("res://interceptor.scn"), load("res://enemy_cleaner.scn")]

var enemy_grid = [
[
[enemies_scenes[0], 0, 3],
[enemies_scenes[1], 15, 10],
[enemies_scenes[2], 30, 2]
]
]

func get_enemies_scenes():
	return enemies_scenes

func get_enemy_grid(level):
	if level < 1:
		return enemy_grid
	else:
		return enemy_grid[level - 1]