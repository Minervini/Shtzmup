
extends KinematicBody2D
var dir = Vector2(1,0)
var velocity = 10.0
var m_dmg = 0
var fire_rate = 0.1
onready var m_Hp = get_node("/root/entities").get_weapons()[0][5]

func _ready():
	add_to_group("a_p")
	dir *= get_parent().get_node("player").get_dir()
	var rot = get_parent().get_node("player").get_rot()
	set_rot(rot)
	dir.y = -(rot + dir.y)
	set_fixed_process(true)
	pass

func _fixed_process(delta):
	if m_Hp == 0:
		destroy()
	move(dir * velocity)
	if is_colliding():
		if get_collider().is_in_group("enemies") or get_collider().is_in_group("e_p"):
			get_collider().attacked(m_dmg)
		queue_free()

func attacked(dmg):
	m_Hp = m_Hp - dmg

func get_fire_rate():
	return fire_rate

func destroy():
	queue_free()
	
func set_dmg(dmg):
	m_dmg = dmg