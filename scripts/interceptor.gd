
extends KinematicBody2D


onready var entities = get_node("/root/entities")
onready var self_param = entities.get_enemies()["Interceptor"]
onready var fire_rate = self_param["fire_rate"]
onready var bullet_scene = self_param["ammos"][0]
onready var destruction_scene = self_param["explosion"]
onready var m_Hp = self_param["hp"]
onready var m_Hp_max = m_Hp
onready var points = m_Hp
onready var m_dmg = self_param["suicide_dmg"]

var player
var player_pos
var dir = Vector2(-1,0)
onready var dir_x = dir.x
onready var dir_y = dir.y
var speed = Vector2(10, 4)
var time = 0
var update_time = 0
var update_freq = 0.2
var shot_time = 0
var explosion_variance = 3

func _ready():
	get_node("hp_bar").set_max(m_Hp_max)
	get_node("hp_bar").set_value(m_Hp * 100 / m_Hp_max)
	player = get_parent().get_node("player")
	add_collision_exception_with(get_parent().get_node("back_blocker"))
	 
	#print(player.get_pos())
	set_fixed_process(true)
	pass
	
func _fixed_process(delta):
	
	#print(fire_rate)
	#print(dir)
	time += delta
	
	if m_Hp <= 0:
		scored()
	if (time - shot_time > fire_rate):
		shot_time = time
		shoot()
		
	if time - update_time > update_freq or time == 0.0:
		update_time = time
		dir_x = (get_pos().x - player._get_last_pos().x)/abs(get_pos().x - player._get_last_pos().x)
		dir_y = (get_pos().y - player._get_last_pos().y)/abs(get_pos().y - player._get_last_pos().y)
	#print("( ",dir_x," , ",dir_y," )")
	#print("time : ", int(time))
		if abs(get_pos().x - player._get_last_pos().x) == 0:
			if get_pos().x - player._get_last_pos().x > 0:
				dir_x = 1
			else:
				dir_x = -1
			if abs(get_pos().y - player._get_last_pos().y) == 0:
				if get_pos().y - player._get_last_pos().y > player._get_last_pos().y + 10 and get_pos().y - player._get_last_pos().y < player._get_last_pos().y - 10:
					dir_y = -1
				else:
					dir_y = 1
		#print(get_pos().y - player._get_last_pos().y)

	#dir = Vector2(dir_x, dir_y)
		dir = Vector2(-dir_x, -dir_y)
		if get_pos().x > player.get_pos().x + 5 and get_node("Sprite").is_flipped_h():
			get_node("Sprite").set_flip_h(false)
		elif get_pos().x < player.get_pos().x - 5 and not get_node("Sprite").is_flipped_h():
			get_node("Sprite").set_flip_h(true)
		
	var motion = dir * speed
	if not (get_pos().x < -50) and not (get_pos().y > 70):
		move(motion)
	else:
		destroy()
		
	if is_colliding():
		if get_collider().is_in_group("players"):
			get_collider().attacked(m_dmg)
			attacked(m_Hp)
			
		if get_collider().is_in_group("collectable"):
			add_collision_exception_with(get_collider())
			
		if get_collider().is_in_group("enemies") or get_collider().is_in_group("undestructible"):
			var n = get_collision_normal()
			motion = n.slide(motion)
			speed = n.slide(speed)
			move(motion * speed)
			
		
		

func shoot():
	var bullet = bullet_scene.instance()
	get_parent().add_child(bullet)
	bullet._set_dir(Vector2(dir.x, 0))
	bullet._set_pos(Vector2(get_pos().x, get_pos().y + 2))
	bullet.add_to_group("enemy_projectile")
	bullet.set_dmg(self_param["ammo_0_parameters"][0]) 
	add_collision_exception_with(bullet)
	
func destroy():
	remove_from_group("enemies")
	get_parent().remove_enemy()
	queue_free()

func attacked(dmg):
	m_Hp = m_Hp - dmg
	get_node("hp_bar").set_value(m_Hp * 100 / m_Hp_max)
	
func scored():
	set_hidden(true)
	for i in range(0,explosion_variance):
		var destr = destruction_scene.instance()
		get_parent().add_child(destr)
		destr.set_pos(get_pos())
	var score = int((points + m_dmg)/fire_rate)
	#print(score)
	get_parent().scoring(score)
	destroy()
	
func set_fire_rate(f_r):
	fire_rate = f_r
func get_fire_rate():
	return fire_rate
